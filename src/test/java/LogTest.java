import java.io.File;

import org.junit.Test;

import com.dgpvp.natey59.minereset.web.PageGenerator;
import com.dgpvp.natey59.minereset.web.Purchase;


public class LogTest {

	@Test
	public void test() {
		
		PageGenerator generator = new PageGenerator(new File("file", "test.html"));
		
		Purchase purchase = new Purchase("natey59", "46*46*46", "no-where");
		
		generator.addPayment(purchase);
		
		generator.removePayment(purchase.getID());
		
		generator.webpage.save();
	}

}
