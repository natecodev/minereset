import org.junit.Test;

import com.dgpvp.natey59.minereset.mine.utils.PrimitiveCuboid;


public class CubiodTest {

	@Test
	public void test() {
		
		long[] high = new long[3];
		long[] low = new long[3];
		
		high[0] = 1;
		high[1] = 1;
		high[2] = 1;
		
		low[0] = 0;
		low[1] = 0;
		low[2] = 0;
		
		PrimitiveCuboid cube = new PrimitiveCuboid(low, high, "test");
		
		System.out.println(cube.getTopLayer().toString());
		
	}

}
