package com.dgpvp.natey59.minereset.runnables;

import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;

public class ResetQueue extends BukkitRunnable {
	
	private MineReset mineReset;
	
	public ResetQueue(MineReset mineReset) {
		this.mineReset = mineReset;
	}
	
	@Override
	public void run() {
		mineReset.getQueue().reloadNext();
	}
	
}
