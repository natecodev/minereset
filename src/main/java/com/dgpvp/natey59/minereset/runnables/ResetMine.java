/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.runnables;

import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class ResetMine extends BukkitRunnable {

	private MineReset plugin;
	private Mine mine;
	
	public ResetMine(MineReset instance, Mine mine) {
		this.plugin = instance;
		this.mine = mine;
	}
	
	@Override
	public void run() {					
		if (mine.getMineArea().getTopLayer().getTypesInCubiod().contains(Material.AIR)) {
			this.plugin.getQueue().addToQueue(mine);
			Messenger.broadcast(mine.getName() + " has been queued.", Permissions.mineQueue);
		}
	}

}
