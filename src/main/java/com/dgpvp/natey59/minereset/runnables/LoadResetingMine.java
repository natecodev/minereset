package com.dgpvp.natey59.minereset.runnables;

import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.mine.Mine;

public class LoadResetingMine extends BukkitRunnable {

	MineReset plugin;
	Mine mine;
	
	public LoadResetingMine(MineReset instance, Mine mine) {
		this.plugin = instance;
		this.mine = mine;
	}
	
	@Override
	public void run() {		
		plugin.createRestingMine(mine);
		
		try {
			mine.convert();
		} catch (Exception e) {
			plugin.log(mine.getName() + " failed to be converted to the new verstion of MineMaterial. This mine should be recreated.");
		}
			
		plugin.getLogger().info(mine.getName() + " has been loaded.");
				
	}
	
	
}
