/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.commands.executor.CommandLoader;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.file.Configuration;
import com.dgpvp.natey59.minereset.file.LogFile;
import com.dgpvp.natey59.minereset.file.backup.Backup;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineID;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.mine.utils.MineParser;
import com.dgpvp.natey59.minereset.mine.utils.Preset;
import com.dgpvp.natey59.minereset.mine.utils.ReloadQueue;
import com.dgpvp.natey59.minereset.runnables.ResetMine;
import com.dgpvp.natey59.minereset.runnables.ResetQueue;
import com.dgpvp.natey59.minereset.web.PageGenerator;
import com.dgpvp.natey59.minereset.web.Purchase;

public class MineReset extends JavaPlugin {

	private List<Mine> mines;
	
	private List<Preset> minePresets;
		
	private HashMap<MineID, Integer> restingMines = new HashMap<MineID, Integer>();
		
	private List<String> blacklist = new ArrayList<String>();
	
	private ReloadQueue reloadQueue;
	
	public HashMap<String, MineResetCommand> commands = new HashMap<String, MineResetCommand>(); //Command label, Command Executor
	
	public List<String> commandNames = new ArrayList<String>();
	
	private File mineSave;
	
	private Configuration config;
	private LogFile log;
	
	private PageGenerator webpage;
	
	@Override
	public void onEnable() {
		this.config = this.loadConfiguration("config.yml");
		this.log = this.loadLog("mine.log");
		
		this.mineSave = new File(this.getDataFolder(), "mines.dat");
				
		this.saveDefaultConfig();
		this.log.save();
		
		MineParser mineParser = new MineParser(this);
		
		try {
			minePresets = Preset.loadPresets(this);
		} catch (MaterialNotFoundException e) {
			e.printStackTrace();
		}
				
		this.mines = mineParser.loadMines();
		
		mineParser.loadMineResets();
		
		CommandLoader loader = new CommandLoader(this);
		
		reloadQueue = new ReloadQueue();
		
		loader.loadCommands();
		
		try {
			blacklist = this.loadBlackList();
		} catch (MaterialNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BukkitRunnable backup = new Backup(this);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, backup, 0L, 36000L);
		
		BukkitRunnable queueRunner = new ResetQueue(this);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, queueRunner, 0L, 2L);
		
		webpage = PageGenerator.create(this);
				
		
	}
	
	@Override
	public void onDisable() {
		
		for (Mine mine : this.mines) {
			
			boolean wasDeleted = false;
			
			if (mine.getName().contains("@test")) {
				this.mines.remove(mine);
				this.log(mine.getName() + " was deleted becuase it was labled as a test mine.");
				wasDeleted = true;
			}
			
			if (wasDeleted == false) {
				for (Entry<MineMaterial, Double> items : mine.getBlocks().entrySet()) {
				
					if (items.getKey().getType() == Material.AIR) {
						this.mines.remove(mine);
						this.log(mine.getName() + " has been deleted since it contains air.");
					}
				
				}
			}
			
		}
		
		this.saveMines();
		
		this.mines.clear();
		this.restingMines.clear();
	}
		
	@Override
	public FileConfiguration getConfig() {
		return this.config.getConfig();
	}
	
	@Override
	public void saveConfig() {
		this.config.save();
	}
	
	@Override
	public void saveDefaultConfig() {
		this.config.saveDefualtConfiguration();
	}
			
	public void addPayment(Purchase payment) {
		
		this.webpage.addPayment(payment);
		
	}
	
	public void removePayment(String id) {
		this.webpage.removePayment(id);
	}
	
	public ReloadQueue getQueue() {
		return this.reloadQueue;
	}
	
	public List<String> loadBlackList() throws MaterialNotFoundException {
		List<String> blacklist = new ArrayList<String>();
		
		for (String string : this.getConfig().getStringList("blacklist")) {
			MineMaterial blackedlistItem;
				
			string = string.replace("%1", ":");
			
			blackedlistItem = MineMaterial.parse(string);
				
			if (blackedlistItem != null) {
				blacklist.add(blackedlistItem.toString());
			}
			
		}
		
		return blacklist;
	}
	
	public List<String> getBlacklist() {
		return this.blacklist;
	}
	
	public void reloadBlacklist() throws MaterialNotFoundException {
		this.blacklist = this.loadBlackList();
	}
	
	private Configuration loadConfiguration(String name) {
		File file = null; //These are null because we let the configuration class take care of initialization
		FileConfiguration fileConfig = null;
		
		return new Configuration(this, file, fileConfig, name);
	}
	
	public LogFile loadLog(String name) {
		File file = new File(this.getDataFolder(), name);
		
		return new LogFile(file);
	}
	
	public LogFile getLog() {
		return this.log;
	}
	
	public void log(String info) {
		String timeStamp = new SimpleDateFormat("MM-dd-yyyy/HH:mm:ss").format(Calendar.getInstance().getTime());
		
		this.getLogger().info(info);
		
		this.log.add(timeStamp + ": " + info);
		this.log.save();
	}
	
	public List<Preset> getPresets() {
		return this.minePresets;
	}
	
	public HashMap<String, MineResetCommand> getCommands() {
		return this.commands;
	}
	
	public void addMine(Mine mine) {
		this.mines.add(mine);
		
		if (this.getConfig().getString("save.frequency").equals("all_creations")) {
			this.saveMines();
		}
	}
	
	public void removeMine(Mine mine) {
		this.mines.remove(mine);
	}
	
	public List<Mine> getUnloadedMines() {
		return this.mines;
	}
	
	public List<Mine> getMines() {
		return this.mines;
	}
		
	public File getMineFile() {
		return this.mineSave;
	}
	
	private void addResetingMine(MineID id, int runnableId) {
		this.restingMines.put(id, runnableId);		
	}
	
	private void addResetingMine(Mine mine, int runnableId) {
		this.addResetingMine(mine.getId(), runnableId);
	}
	
	public synchronized void createRestingMine(Mine mine) {
		
		BukkitRunnable resetMine = new ResetMine(this, mine);
		
		int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, resetMine, 0, mine.getTimeToReset());
		
		this.addResetingMine(mine, id);
		
	}
	
	public void removeRestingMine(Mine mine) {
		int id = this.restingMines.get(mine.getId());
		
		Bukkit.getScheduler().cancelTask(id);
		
		this.restingMines.remove(mine.getId());
	}
	
	public HashMap<MineID, Integer> getResettingMines() {
		return this.restingMines;
	}
	
	public void addCommand(MineResetCommand command) {
		this.commands.put(command.getLabel(), command);
		this.commandNames.add(command.getLabel());
	}
	
	public String generateErrorReport(Exception stackTraceElement, String header) {
		
		SecureRandom random = new SecureRandom();
		
		String errorId = new BigInteger(130, random).toString(32);
		
		LogFile errorLog = this.loadLog(File.separator + "error" + File.separator + errorId + ".elog");
		
		errorLog.add("This is error log: " + errorId);
		errorLog.add(header + "\n\n");
		
		errorLog.add(stackTraceElement.getLocalizedMessage() + "\n");
		
		
		
		for (StackTraceElement element : stackTraceElement.getStackTrace()) {
			errorLog.add(element.toString());
		}
		
		errorLog.save();
		
		return errorId;
	}
	
	public String generateErrorReport(String header) {
		
		SecureRandom random = new SecureRandom();
		
		String errorId = new BigInteger(130, random).toString(32);
		
		LogFile errorLog = this.loadLog("\\error\\" + errorId + ".elog");
		
		errorLog.add("This is error log: " + errorId);
		errorLog.add(header + "\n\n");
				
		errorLog.save();
		
		return errorId;
	}
	
	public void saveMines() {
		
		try {
			this.mineSave.createNewFile();
			
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(this.mineSave));
			
			outputStream.writeObject(this.mines);
			
			this.getLogger().info(this.mines.size() + " mines saved.");
			
			outputStream.flush();
			outputStream.close();
			
		} catch (IOException e) {	
			
			this.getLog().add("Failed to save mines.");
			this.getLogger().log(Level.SEVERE, "MINE.DAT FAILED TO SAVE! THERE WILL BE NO MINES SAVED. PLEASE EMAIL" + Calendar.DATE + "-ERROR.log TO NATEY59@GMAIL.COM.");
			this.getLogger().log(Level.SEVERE, "PLUGIN SHUTTING DOWN FOR INTEGREITY OF ALL SURVIVING MINES.");
			
			this.getServer().getPluginManager().disablePlugin(this);
						
			LogFile errorLog = new LogFile(new File(this.getDataFolder(), Calendar.DATE + "-ERROR.log"));
			
			for (StackTraceElement trace : e.getStackTrace()) {
				errorLog.add(trace.toString());
			}
			
			errorLog.save();
			
		}
//			int count = 0;
//			
//			for (Mine mine : this.mines) {
//				
//				try {
//					File mineSave = new File(this.getDataFolder(), "\\" + "mines" + "\\" + mine.getName() + ".dat");
//					
//					mineSave.createNewFile();
//					
//					ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(mineSave));
//					
//					output.flush();
//					
//					output.writeObject(mine);
//									
//					output.flush();
//					output.close();
//				
//					count++;
//				} catch (IOException e) {
//					
//					this.log(mine.getName() + " could not be saved.");
//					this.generateErrorReport(e, "Failed to save " + mine.getName() + ".");
//					
//				}
//			}
//			
//			this.log(count + " mines saved.");
	
	}
	
	public void saveMine() {
		
	}
	
	public Preset getPresetByName(String name) {
		
		Preset presetToGive = null;
		
		for (Preset preset : this.minePresets) {
			
			if (preset.getName().equalsIgnoreCase(name)) {
				presetToGive = preset;
				break;
			}
			
		}
		
		return presetToGive;
	}
	 
}