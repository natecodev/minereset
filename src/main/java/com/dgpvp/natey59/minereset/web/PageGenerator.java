package com.dgpvp.natey59.minereset.web;

import java.io.File;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.file.LogFile;

public class PageGenerator {

	public LogFile webpage;
	
	public PageGenerator(File file) {
		webpage = new LogFile(file);
	}
	
	public void addPayment(Purchase payment) {
				
		String element = this.createElement(payment);
		
		webpage.add(element);
		webpage.save();
	}
	
	public void removePayment(String id) {
				
		String itemToRemove = "";
		
		for (String element : this.webpage.getValues()) {
			
			if (element.startsWith("<div id=\"" + id + "\"")) {
				itemToRemove = element;
			}
			
		}
		
		this.webpage.getValues().remove(itemToRemove);
		
		this.webpage.save();
		
	}
	
	public void addJS() {
		webpage.add("<head> <title>Generated Payment Page</title> <script src=\"deleter.js\"></script> </head>");
		
		webpage.save();
	}
	
	public static PageGenerator create(MineReset mineReset) {
		File file = new File(mineReset.getDataFolder(), "payments.html");
		
		PageGenerator generator = new PageGenerator(file);
		
		generator.addJS();
		
		return generator;
	}
	
	private String createElement(Purchase payment) {
		
		String id = payment.getID();
		
		String element = "<div id=\"" + id + "\"> <div>";
		
		element += "Purchaser: " +payment.getPurchaser() + "<br>";
		element += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Id: " + id + "<br>";
		element += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Size: " + payment.getSize() + "<br>";
		element += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location: " + payment.getLocation() + "<br>";
		element += "";
		
		element += "</div></div><input id=\"clickMe\" type=\"button\" value=\"DELETE\" onclick=\"removeDiv(\'" + id +  "\');\" /><br><br>";
		
		return element;
	}
	
}
