package com.dgpvp.natey59.minereset.web;

import java.util.UUID;

public class Purchase {

	private String purchaser;
	private String size;
	private String location;
	private String id;
	
	public Purchase(String player, String size, String location) {
		this.purchaser = player;
		this.size = size;
		this.location = location;
		this.id = UUID.randomUUID().toString().replace("-", "");
	}
	
	public String getPurchaser() {
		return this.purchaser;
	}
	
	public String getSize() {
		return this.size;
	}
	
	public String getLocation() {
		return this.location;
	}

	public String getID() {
		return id;
	}
	
}
