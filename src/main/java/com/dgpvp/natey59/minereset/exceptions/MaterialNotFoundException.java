package com.dgpvp.natey59.minereset.exceptions;

public class MaterialNotFoundException extends Exception {

	private static final long serialVersionUID = -309640980429522456L;

	public MaterialNotFoundException(String message) {
		super(message);
	}

	public MaterialNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
