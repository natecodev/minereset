/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.language;

import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;

public class Messenger {

	public static void resultMessage(Player player, String message) {
		player.sendMessage(prepareMessage(ChatColor.GREEN + message));
	}
	
	public static void errorMessage(Player player, String message) {
		
		player.sendMessage(Messenger.prepareMessage(ChatColor.RED + message));
		
	}
	
	public static void noPermissionMessage(Player player) {
		player.sendMessage(Messenger.prepareMessage(ChatColor.RED + "You don't have permission to use that command!"));
	}
	
	public static String prepareMessage(String message) {
		
		message = addHeader(message);
		message = translateColors(message);
		
		return message;
	}
	
	private static String translateColors(String message) {
		
		message = ChatColor.translateAlternateColorCodes('&', message);
		message = ChatColor.translateAlternateColorCodes('$', message);
		
		return message;
		
	}
	
	private static String addHeader(String message) {
		
		message = "&3[&4MineReset&3]: " + ChatColor.WHITE + message;
		
		return message;
		
	}

	public static void sendMineInfo(Player player, Mine mine) {
		Messenger.resultMessage(player, "Information for " + mine.getName() + "\n");
		
		double timeToReset = ((mine.getTimeToReset()/20)/60);
		
		String mineID = "&3    [&4ID&3]: " + ChatColor.BOLD + mine.getId().toString();
		String owner = "&3    [&4Owner&3]: " + ChatColor.BOLD + mine.getOwner().getName();
		String resetTime = (timeToReset > 0) ? "&3    [&4Reset Time&3]: " + ChatColor.BOLD + timeToReset + " minutes" : "&3    [&4Reset Time&3]: " + ChatColor.BOLD + "is less than 1 minute or this mine is part of a cluster";
		String blockInfo = "&r    [&4Blocks&3]: " + ChatColor.BOLD;
		
		for (Entry<MineMaterial, Double> blocks : mine.getBlocks().entrySet()) {
			
			blockInfo += blocks.getKey().toString() + ":" + blocks.getValue() + "% ";
			
		}
		
		mineID = translateColors(mineID);
		owner = translateColors(owner);
		resetTime = translateColors(resetTime);
		blockInfo = translateColors(blockInfo);
		
		player.sendMessage(mineID + "\n" + owner + "\n" + resetTime + "\n" + blockInfo);
		
		
	}
	
	

	public static void sendNotes(Player player, Mine mine) {
		
		Messenger.resultMessage(player, "Notes for " + mine.getName() + ":");
		
		for (String note : mine.getNotes()) {
			player.sendMessage(Messenger.translateColors("    &4 - " + note));
		}
		
	}
	
	public static void sendHelp(MineReset mr, Player player) {
		
		String commands = "Here are all the commands you have access to: \n";
		
		for (Entry<String, MineResetCommand> commandEntry : mr.getCommands().entrySet()) {
			
			if (player.hasPermission(commandEntry.getValue().getPermission())) {
				commands += "&3- &2" + commandEntry.getKey() + "\n     " + "usage: " + commandEntry.getValue().getUsage() + "\n     " + "description: " + commandEntry.getValue().getDescription() + "\n";
			}
			
		}
		
		commands = translateColors(commands);
		
		player.sendMessage(commands);
		
	}
	
	public static void broadcast(String message, Permission perm) {
		
		message = Messenger.addHeader(message);
		message = Messenger.translateColors(message);
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			
			if (player.hasPermission(perm)) {
				
				player.sendMessage(message);
				
			}
			
		}
		
	}
	
}
