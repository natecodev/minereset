package com.dgpvp.natey59.minereset.language;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineParser;

public class SendPlayerMineInfo extends BukkitRunnable {

	private MineReset mineReset;
	private Player sendTo;
	private OfflinePlayer about;
	
	public SendPlayerMineInfo(MineReset mineReset, Player sender, OfflinePlayer about) {
		this.mineReset = mineReset;
		this.sendTo = sender;
		this.about = about;
	}
	
	@Override
	public void run() {
			
			MineParser mineParser = new MineParser(mineReset);
			
			String message = about.getName() + " owns to following mine(s): &l";
			String mines = "";
			
			for (Mine mine : mineParser.getPlayersMines(about)) {
				mines += mine.getName() + ", ";
			}
			
			message = message + mines;
			
			message = Messenger.prepareMessage(message);
			
			sendTo.sendMessage(message);
		
	}

}
