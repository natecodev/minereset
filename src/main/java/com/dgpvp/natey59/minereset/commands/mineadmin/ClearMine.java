package com.dgpvp.natey59.minereset.commands.mineadmin;

import java.util.Map.Entry;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class ClearMine extends MineResetCommand {

	public ClearMine(MineReset mineReset) {
		super(mineReset, "clear");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr clear");
		this.setParameterCount(0);
		this.setDescription("Remove all the blocks from a mine.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mine = MineSelector.getMinePlayerIsOn(mineReset, (Player)sender);
		
		if (mine != null) {
			
			for (Entry<MineMaterial, Double> blocks : mine.getBlocks().entrySet()) {
				
				mine.removeBlock(blocks.getKey());
				
			}
			
			Messenger.resultMessage((Player)sender, "This mine has been cleared.");
			
		}
		
		
	}

	
	
}
