package com.dgpvp.natey59.minereset.commands.selector;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class HereCommand extends MineResetCommand {

	public HereCommand(MineReset mineReset) {
		super(mineReset, "here");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr here");
		this.setParameterCount(0);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine minePlayerIsOn = MineSelector.getMinePlayerIsOn(mineReset, (Player) sender);
		
		if (minePlayerIsOn != null) {
			Messenger.resultMessage((Player) sender, "You are standing on " + minePlayerIsOn.getName());
		} else {
			Messenger.errorMessage((Player) sender, "You are either not standing on a mine or the mine you are standing on is unloaded. Break a block in the mine to load it.");
		}
		
	}

	
	
}
