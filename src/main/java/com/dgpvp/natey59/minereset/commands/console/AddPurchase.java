package com.dgpvp.natey59.minereset.commands.console;

import java.util.Arrays;

import org.bukkit.command.CommandSender;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.permissions.Permissions;
import com.dgpvp.natey59.minereset.web.Purchase;

public class AddPurchase extends MineResetCommand {

	public AddPurchase(MineReset mineReset) {
		// /mr purchase <user> <mine-type>
		super(mineReset, "denotepurchase");
		this.setParameterCount(3);
		this.setPermission(Permissions.console);
		this.setUsage("/mr purchase <user> <mine-type> <location>");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		String location = "";
		
		for (String string : Arrays.copyOfRange(args, 2, args.length)) {
			location += string + " ";
		}
		
		Purchase newPurchase = new Purchase(args[0], args[1], location);
		
		mineReset.addPayment(newPurchase);
	}

	
	
}
