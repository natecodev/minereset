package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineParser;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class WarpToMine extends MineResetCommand {

	public WarpToMine(MineReset mineReset) {
		super(mineReset, "warp");
		this.setUsage("/mr warp <mine>");
		this.setPermission(Permissions.mineAdmin);
		this.setParameterCount(1);
		this.setDescription("Teleport to a mine.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mine = MineParser.getMineByName(mineReset, args[0]);
		
		if (mine != null) {
			((Player) sender).teleport(mine.getWarp());
		} else {
			Messenger.errorMessage((Player) sender, "The mine " + args[0] + " could not be found.");
		}
		
		
	}

	
	
}
