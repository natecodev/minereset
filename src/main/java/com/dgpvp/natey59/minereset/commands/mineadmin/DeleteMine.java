package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class DeleteMine extends MineResetCommand {

	public DeleteMine(MineReset mineReset) {
		super(mineReset, "delete");
		this.setPermission(Permissions.mineAdmin);
		this.setParameterCount(0);
		this.setUsage("/mr delete");
		this.setDescription("Delete a mine.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mineToDelete = MineSelector.getSelectedMineFor(mineReset, (Player) sender);
		
		if (mineToDelete != null) {
			
			mineReset.removeMine(mineToDelete);
			mineReset.removeRestingMine(mineToDelete);
			
			Messenger.resultMessage((Player)sender, mineToDelete.getName() + " has been deleted.");
			
		} else {
			Messenger.errorMessage((Player) sender, "You are not standing on a mine and you don't have a mine selected.");
		}
		
	}

}
