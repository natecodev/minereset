package com.dgpvp.natey59.minereset.commands.selector;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class PositionalSeclector extends MineResetCommand{

	public static HashMap<Player, Location> positionOne = new HashMap<Player, Location>();
	public static HashMap<Player, Location> positionTwo = new HashMap<Player, Location>();
	
	public PositionalSeclector(MineReset mineReset) {
		super(mineReset, "pos");
		this.setPermission(Permissions.createMine);
		this.setUsage("/minereset pos <number>");
		this.setParameterCount(1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		int position = Integer.parseInt(args[0]);
		
		Player player = (Player) sender;
				
		if (position == 1) {
			Location pos1 = player.getLocation();
			
			PositionalSeclector.positionOne.put(player, pos1);
			
			Messenger.resultMessage(player, "Position one has been set to " + pos1.getBlockX() + "," + pos1.getBlockY() + "," + pos1.getBlockZ());
		} else if (position == 2) {
			
			Location pos2 = player.getLocation();
			
			PositionalSeclector.positionTwo.put(player, pos2);
			
			Messenger.resultMessage(player, "Position two has been set to " + pos2.getBlockX() + "," + pos2.getBlockY() + "," + pos2.getBlockZ());
		}
	}

}
