package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class ForceReset extends MineResetCommand {

	public ForceReset(MineReset mineReset) {
		super(mineReset, "reset");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr reset");
		this.setParameterCount(0);
		this.setDescription("Force a mine to reset.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mineToReset = MineSelector.getSelectedMineFor(mineReset, (Player)sender);
				
		mineReset.getQueue().addToQueue(mineToReset);
		
		Messenger.resultMessage((Player) sender, mineToReset.getName() + " has been reset.");
		
	}

	
	
}
