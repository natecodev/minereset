package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class Count extends MineResetCommand {

	public Count(MineReset mineReset) {
		super(mineReset, "count");
		this.setDescription("Count the number of mines.");
		this.setParameterCount(0);
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr count");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Messenger.resultMessage((Player)sender, "There are " + mineReset.getMines().size() + " mines.");
		
	}

	
	
}
