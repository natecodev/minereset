package com.dgpvp.natey59.minereset.commands.mineadmin;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.utils.Preset;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class CreatePreset extends MineResetCommand {

	public CreatePreset(MineReset mineReset) {
		super(mineReset, "mp");
		this.setUsage("/mr mp <name> <preset>");
		this.setParameterCount(2);
		this.setPermission(Permissions.createPreset);
		this.setDescription("Create a material preset.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		//Example: /mr mp a_woody [17,17:1,17:2,17:3,162,162:1]
		
		String name = args[0];
		
		String items = args[1];
		
		String formated = name + "{" + items + "}";
		
		Preset newPreset;
		try {
			newPreset = Preset.parseFromConfig(formated);
			
			List<String> currentPresets = mineReset.getConfig().getStringList("presets");
			
			currentPresets.add(newPreset.toString());
			
			mineReset.getConfig().set("presets", currentPresets);		
			
			Messenger.resultMessage((Player) sender, "You have created a new preset called: " + newPreset.getName());
			
			mineReset.saveConfig();
		} catch (MaterialNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}

	
	
}
