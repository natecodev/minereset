package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.file.backup.Backup;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class ForceBackup extends MineResetCommand {

	public ForceBackup(MineReset mineReset) {
		super(mineReset, "backup");
		this.setPermission(Permissions.backup);
		this.setDescription("Force a mines.dat backup");
		this.setParameterCount(0);
		this.setUsage("/minereset backup");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		BukkitRunnable backup = new Backup(mineReset);
		
		backup.runTaskAsynchronously(mineReset);
		
		
	}

	
	
}
