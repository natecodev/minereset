package com.dgpvp.natey59.minereset.commands.mineadmin;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class BlacklistBlock extends MineResetCommand {

	public BlacklistBlock(MineReset mineReset) {
		super(mineReset, "blacklist");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr blacklist <block>");
		this.setParameterCount(1);
		this.setDescription("Blacklist a block.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		try {
			MineMaterial materialToBlacklist = null;
			
			materialToBlacklist = MineMaterial.parse(args[0]);
							
			List<String> blacklist = mineReset.getConfig().getStringList("blacklist");
				
			blacklist.add(materialToBlacklist.toString().replace(":", "%1"));
				
			mineReset.getConfig().set("blacklist", blacklist);			
				
			mineReset.saveConfig();
				
			mineReset.reloadBlacklist();
				
			Messenger.resultMessage((Player) sender, materialToBlacklist.toString() + " has been blacklisted. This means only admins can add it to a mine.");

		} catch (MaterialNotFoundException e) {
			Messenger.errorMessage((Player) sender, args[0] + " is not a valid material type.");
		}
		
	}

	
	
}
