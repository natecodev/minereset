package com.dgpvp.natey59.minereset.commands.executor;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.language.Messenger;

public class MineResetCommandExecutor implements CommandExecutor {

	private MineReset mineReset;
	
	public MineResetCommandExecutor(MineReset mineReset) {
		this.mineReset = mineReset;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {		
		
		try {
			if (args.length >= 1) {
				MineResetCommand command = this.getCommand(args[0]);
				
				if (command != null) {
					if ((args.length - 1) >= command.getParameterCount()) {
						
						boolean userCanRun = false;
						Permission permission = command.getPermission();
						
						if (permission != null) {
							userCanRun = sender.hasPermission(permission);
						} else {
							userCanRun = true;
						}
					
						if (userCanRun) {
							String[] arguments = Arrays.copyOfRange(args, 1, args.length);
						
							command.execute(sender, arguments);
						} else {
							//Send no permission
							Messenger.noPermissionMessage((Player) sender);
						}
					} else {
						//Not enough parameters.
						Messenger.errorMessage((Player)sender, command.getUsage());
					}
				} else {
					//Send other commands.
					Messenger.errorMessage((Player)sender, "The command you entered could not be found. Try /minereset help to get all the commands.");
				}
			} 
		}catch (Exception e) {
			
			Player player = (Player) sender;
			
			this.generateErrorReport(player, args, e);
			
			
		}
		
		return true;
	}
	
	public MineResetCommand getCommand(String label) {
		
		return this.mineReset.commands.get(label);
		
	}
	
	public void generateErrorReport(Player player, String[] args, Exception e) {
		String header = "";
		
		if (args.length < 1) {
			header = "This error was generated when the user " + player.getName() + " (" + player.getUniqueId() + ") ran the minereset command with no parameters.";
		} else {
			header = "This error was generated when the user " + player.getName() + " (" + player.getUniqueId() + ") attempted to execute /minereset " + args[0] + "."
					+ " Below is a map of all the arguments given. \n\n Arguments: \n";
			for (String argument : args) {
				header += argument + "\n";
			}
			
		}
		
		String id = mineReset.generateErrorReport(e, header);
		
		
		Messenger.errorMessage(player, "Something has gone horribly wrong! Please take a snapshot of this message and send it to an admin. ID: " + id);
	}

}
