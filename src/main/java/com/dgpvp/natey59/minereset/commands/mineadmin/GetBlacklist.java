package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class GetBlacklist extends MineResetCommand {

	public GetBlacklist(MineReset mineReset) {
		super(mineReset, "gblacklist");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr gblacklist");
		this.setParameterCount(0);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		for (String material : mineReset.getBlacklist()) {
			sender.sendMessage(material);
		}
				
	}

}
