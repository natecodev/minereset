package com.dgpvp.natey59.minereset.commands.executor;

import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;

import com.dgpvp.natey59.minereset.MineReset;

public abstract class MineResetCommand {

	protected MineReset mineReset;
	protected Permission permission;
	protected String label;
	protected String usage;
	protected int parameters;
	protected String[] aliases;
	protected String description;
	
	public MineResetCommand(MineReset mineReset, String label) {
		this.mineReset = mineReset;
		this.label = label;
		this.permission = null;
		this.usage = "";
		this.parameters = 0;
		
	}
	
	public abstract void execute(CommandSender sender, String[] args);
		
	public void setParameterCount(int count) {
		this.parameters = count;
	}
	
	public int getParameterCount() {
		return this.parameters;
	}
	
	public void setPermission(Permission permission) {
		this.permission = permission;
	}
	
	public Permission getPermission() {
		return this.permission;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public void setUsage(String usage) {
		this.usage = usage;
	}
	
	public String getUsage() {
		return this.usage;
	}

	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	
}
