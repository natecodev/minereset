package com.dgpvp.natey59.minereset.commands.mineuser;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class MineUserAddBlock extends MineResetCommand {

	public MineUserAddBlock(MineReset mineReset) {
		super(mineReset, "uadd");
		this.setUsage("/mr addu <block> <percentage>");
		this.setParameterCount(2);
		this.setPermission(Permissions.mineUser);
		this.setDescription("Add a block to a mine");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mineToEdit = MineSelector.getSelectedMineFor(mineReset, (Player)sender);
		
		if (mineToEdit != null) {
			
			if ((mineToEdit.getOwner().isOnline()) && (mineToEdit.getOwner().getPlayer() == ((Player)sender))) {
				
				try {
					MineMaterial material = MineMaterial.parse(args[0]);
					Double percentage = 0D;
					
					try {
						percentage = Double.parseDouble(args[1]);
					} catch (Exception e) {
						Messenger.errorMessage((Player) sender, "You did not enter a valid percentage.");
						return;
					}
					
					if (!mineReset.getBlacklist().contains(material.toString())) {
						
						boolean wasAdded;
						
						wasAdded = mineToEdit.addBlock(percentage, material);
						
						if (wasAdded == true) {
							Messenger.resultMessage((Player)sender, "Material successfully added.");
						} else {
							Messenger.errorMessage((Player) sender, "Your mine already has 100% of the types full. Use /minereset uremove to remove something.");
						}
						
					} else {
						Messenger.errorMessage((Player)sender, "Sorry, the block type you wish to add in your mine is not permitted.");
						return;
					}
				} catch (MaterialNotFoundException e) {
					Messenger.errorMessage((Player)sender, "You did not enter a valid block type.");
				}
				
				
			} else {
				Messenger.errorMessage((Player)sender, "You cannot edit a mine you do not own.");
			}
			
		} else {
			Messenger.errorMessage((Player) sender, "You are not standing on a mine.");
		}
		
	}

	
	
}
