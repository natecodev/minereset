package com.dgpvp.natey59.minereset.commands.selector;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class MineDeselctor extends MineResetCommand {

	public MineDeselctor(MineReset mineReset) {
		super(mineReset, "deselect");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr deselect");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Player player = (Player) sender;
		
		MineSelector.getSelectedMines().remove(player);
		
		Messenger.resultMessage(player, "You no longer have a mine selected.");
	}

}
