package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class GetCreator extends MineResetCommand {

	public GetCreator(MineReset mineReset) {
		super(mineReset, "getcreator");
		this.setParameterCount(0);
		this.setDescription("Get the creator of a mine");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr getcreator");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mine = MineSelector.getSelectedMineFor(mineReset, (Player)sender);
		
		if (mine != null) {
			
			Messenger.resultMessage((Player)sender, "The creator of this mine is: &4"  + mine.getCreator());
			
		} else {
			
			Messenger.errorMessage((Player)sender, "There is no mine under you to perform the check on.");
			
		}
		
	}

	
	
}
