package com.dgpvp.natey59.minereset.commands.executor;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.console.AddPurchase;
import com.dgpvp.natey59.minereset.commands.mineadmin.AddBlockToMine;
import com.dgpvp.natey59.minereset.commands.mineadmin.BlacklistBlock;
import com.dgpvp.natey59.minereset.commands.mineadmin.ClearMine;
import com.dgpvp.natey59.minereset.commands.mineadmin.Count;
import com.dgpvp.natey59.minereset.commands.mineadmin.CreatePreset;
import com.dgpvp.natey59.minereset.commands.mineadmin.DeleteMine;
import com.dgpvp.natey59.minereset.commands.mineadmin.ForceBackup;
import com.dgpvp.natey59.minereset.commands.mineadmin.ForceReset;
import com.dgpvp.natey59.minereset.commands.mineadmin.GetBlacklist;
import com.dgpvp.natey59.minereset.commands.mineadmin.GetCreator;
import com.dgpvp.natey59.minereset.commands.mineadmin.GetMineInfo;
import com.dgpvp.natey59.minereset.commands.mineadmin.GetPresets;
import com.dgpvp.natey59.minereset.commands.mineadmin.PlayerWhois;
import com.dgpvp.natey59.minereset.commands.mineadmin.RemoveBlockFromMine;
import com.dgpvp.natey59.minereset.commands.mineadmin.WarpToMine;
import com.dgpvp.natey59.minereset.commands.minecreation.CreateMine;
import com.dgpvp.natey59.minereset.commands.mineuser.Help;
import com.dgpvp.natey59.minereset.commands.mineuser.MineUserAddBlock;
import com.dgpvp.natey59.minereset.commands.mineuser.MineUserRemoveBlock;
import com.dgpvp.natey59.minereset.commands.selector.HereCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineDeselctor;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.commands.selector.PositionalSeclector;

public class CommandLoader {

	private MineReset mineReset;
	
	public CommandLoader(MineReset mineReset) {
		this.mineReset = mineReset;
	}
	
	public void loadCommands() {
		
		//Load primary command executor
		mineReset.getCommand("minereset").setExecutor(new MineResetCommandExecutor(mineReset));
		
		//Load all sub-commands
		mineReset.addCommand(new PositionalSeclector(mineReset));
		mineReset.addCommand(new CreateMine(mineReset));
		mineReset.addCommand(new MineSelector(mineReset));
		mineReset.addCommand(new MineDeselctor(mineReset));
		mineReset.addCommand(new AddBlockToMine(mineReset));
		mineReset.addCommand(new RemoveBlockFromMine(mineReset));
		mineReset.addCommand(new GetMineInfo(mineReset));
		mineReset.addCommand(new HereCommand(mineReset));
		mineReset.addCommand(new CreatePreset(mineReset));
		mineReset.addCommand(new GetPresets(mineReset));
		mineReset.addCommand(new Help(mineReset));
		mineReset.addCommand(new WarpToMine(mineReset));
		mineReset.addCommand(new PlayerWhois(mineReset));
		mineReset.addCommand(new DeleteMine(mineReset));
		mineReset.addCommand(new ForceReset(mineReset));
		mineReset.addCommand(new ClearMine(mineReset));
		mineReset.addCommand(new BlacklistBlock(mineReset));
		mineReset.addCommand(new MineUserAddBlock(mineReset));
		mineReset.addCommand(new MineUserRemoveBlock(mineReset));
		mineReset.addCommand(new GetBlacklist(mineReset));
		mineReset.addCommand(new ForceBackup(mineReset));
		mineReset.addCommand(new AddPurchase(mineReset));
		mineReset.addCommand(new GetCreator(mineReset));
		mineReset.addCommand(new Count(mineReset));		
	}
	
}
