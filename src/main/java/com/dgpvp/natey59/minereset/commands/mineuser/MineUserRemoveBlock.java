package com.dgpvp.natey59.minereset.commands.mineuser;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class MineUserRemoveBlock extends MineResetCommand {

	public MineUserRemoveBlock(MineReset mineReset) {
		super(mineReset, "uremove");
		this.setPermission(Permissions.mineUser);
		this.setUsage("/mr uremove <block>");
		this.setDescription("Remove a block from a mine");
		this.setParameterCount(1);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mineToEdit = MineSelector.getSelectedMineFor(mineReset, (Player)sender);
		
		if (mineToEdit != null) {
			
			if ((mineToEdit.getOwner().isOnline()) && (mineToEdit.getOwner().getPlayer() == ((Player)sender))) {
				
				try {
					MineMaterial material = MineMaterial.parse(args[0]);
										
					mineToEdit.removeBlock(material);
					
					Messenger.resultMessage((Player)sender, material.toString() + " has been removed from your mine.");
					
				} catch (MaterialNotFoundException e) {
					Messenger.errorMessage((Player)sender, "You did not enter a valid block type.");
				}
				
				
			} else {
				Messenger.errorMessage((Player)sender, "You cannot edit a mine you do not own.");
			}
			
		} else {
			Messenger.errorMessage((Player) sender, "You are not standing on a mine.");
		}
		
	}

}
