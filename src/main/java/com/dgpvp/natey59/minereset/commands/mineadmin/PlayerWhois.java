package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.language.SendPlayerMineInfo;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class PlayerWhois extends MineResetCommand {

	public PlayerWhois(MineReset mineReset) {
		super(mineReset, "whois");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr whois <player>");
		this.setParameterCount(1);
		this.setDescription("Lookup what mines a player owns.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		
		@SuppressWarnings("deprecation")
		OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
		
		BukkitRunnable sendInfo = new SendPlayerMineInfo(mineReset, (Player) sender, player);
		
		Messenger.resultMessage((Player)sender, "You will recieve the information for " + player.getName() + " once all " + mineReset.getMines().size() + " mines are searched through."
				+ "(This can take a while)");
		
		sendInfo.runTaskAsynchronously(mineReset);
	}

	
	
}
