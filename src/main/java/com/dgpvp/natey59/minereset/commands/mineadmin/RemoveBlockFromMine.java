package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class RemoveBlockFromMine extends MineResetCommand {

	public RemoveBlockFromMine(MineReset mineReset) {
		super(mineReset, "remove");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr remove material");
		this.setParameterCount(1);
		this.setDescription("Remove a block from a mine.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Player player = (Player) sender;
		
		Mine mine = MineSelector.getSelectedMineFor(mineReset, player);
		
		if (mine != null) {
			MineMaterial material;
			try {
				material = MineMaterial.parse(args[0].toUpperCase());
				
				mine.removeBlock(material);
				
				Messenger.resultMessage(player, material.getType().name() + " has been removed from " + mine.getName());
			} catch (MaterialNotFoundException e) {
				Messenger.errorMessage(player, "You did not enter a valid material.");
			}
		
				
				
		} else {
			Messenger.errorMessage(player, "You do not have a mine selected. This is becuase you are either not standing on one, you didn't do /mr select <mine>, "
					+ "or the mine you are standing on has not been enabled. To enable it just break a block inside of it.");
		}
		
	}

	
	
}
