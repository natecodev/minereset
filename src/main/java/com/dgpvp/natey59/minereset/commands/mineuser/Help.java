package com.dgpvp.natey59.minereset.commands.mineuser;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class Help extends MineResetCommand {

	public Help(MineReset mineReset) {
		super(mineReset, "help");
		this.setParameterCount(0);
		this.setUsage("/mr help");
		this.setPermission(Permissions.mineAdmin);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Messenger.sendHelp(mineReset, (Player) sender);
		
	}

	
	
}
