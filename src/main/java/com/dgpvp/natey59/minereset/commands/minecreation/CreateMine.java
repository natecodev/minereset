package com.dgpvp.natey59.minereset.commands.minecreation;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.commands.selector.PositionalSeclector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineID;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.mine.utils.Preset;
import com.dgpvp.natey59.minereset.mine.utils.PrimitiveCuboid;
import com.dgpvp.natey59.minereset.mine.utils.TimeParser;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class CreateMine extends MineResetCommand {

	public CreateMine(MineReset mineReset) {
		super(mineReset, "create");
		this.setPermission(Permissions.createMine);
		this.setUsage("/minereset create <name> <owner> <reset-time> <optional: block(s)> <optional: flags>");
		this.setParameterCount(3);
		this.setDescription("Create a mine.");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Player player = (Player) sender;
		
		Location pos1 = PositionalSeclector.positionOne.get(player);
		Location pos2 = PositionalSeclector.positionTwo.get(player);
		
		
		if (pos1 == null || pos2 == null) {
			Messenger.errorMessage(player, "You do not have two positions selected.");
			return;
		}
		
		PositionalSeclector.positionOne.remove(player);
		PositionalSeclector.positionTwo.remove(player);
		
		PrimitiveCuboid cube = new PrimitiveCuboid(pos1, pos2);
		
		String name = args[0];
		
		OfflinePlayer owner = Bukkit.getOfflinePlayer(args[1]);
		
		long resetTime = 0;
		
		try {
			resetTime = TimeParser.parseStringToLong(args[2]);
		} catch (NumberFormatException e) {
			Messenger.errorMessage((Player)sender, "Illegal time format.");
			return;
		}
		
		List<MineMaterial> intialBlocks = new ArrayList<MineMaterial>();
		
		if (args.length >= 4) {
			
			if (args[3].contains(",")) {
				
				String[] data = args[3].split(",");
				
				for (String item : data) {
					
					item = item.replace(" ", "");
					
					if (!item.equals(" ") && !item.equals("") && item != null) {
						
						try {
							MineMaterial material = MineMaterial.parse(item.toUpperCase());
							
							if (material != null) {
								intialBlocks.add(material);
							}
						} catch (Exception e) {
							Messenger.errorMessage((Player) sender, "The type " + item + " could not be found.");
						}
						
					}
					
				}
				
			} else if (args[3].contains("ps:")) {
				
				String presetString = args[3].replace("ps:", "");
				
				Preset preset = mineReset.getPresetByName(presetString);
				
				intialBlocks = preset.getMaterials();
				
				sender.sendMessage("Using preset: " + preset.getName());
				
				
			} else {
				
				try {
					MineMaterial material = MineMaterial.parse(args[3].toUpperCase());
					
					if (material != null) {
						intialBlocks.add(material);
					}
					
				} catch (Exception e){
					
				}
				
			}
			
		}
		
		if (args.length >= 5) {
			
		}
		
		double y = ((Player)sender).getWorld().getHighestBlockYAt(pos1);
		Location warp = pos1;
		pos1.setY(y);
		
		Mine mine = new Mine(MineID.generateMineId(), name, owner, sender.getName(), (int) resetTime, cube, warp);
		
		if (intialBlocks.size() >= 1) {
			
			double percentageEach = 100/intialBlocks.size();
			
			for (MineMaterial material : intialBlocks) {
				mine.addBlock(percentageEach, material);
			}
			
		}
		
		mineReset.createRestingMine(mine);
		
		mineReset.addMine(mine);
		
		mine.reload(true);
		
		MineSelector.selectedMines.remove(player.getUniqueId());
		MineSelector.selectedMines.put(player.getUniqueId(), mine.getId());
		
		Messenger.resultMessage(player, "A mine called " + name + " has been created.");
		
	}
	
}































