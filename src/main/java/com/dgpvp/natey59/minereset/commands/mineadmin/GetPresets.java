package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.utils.Preset;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class GetPresets extends MineResetCommand {

	public GetPresets(MineReset mineReset) {
		super(mineReset, "presets");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr presets");
		this.setParameterCount(0);
		this.setDescription("Get a list of all the Material presets.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Player player = (Player) sender;
				
		String presetList = "";
		
		for (Preset preset : mineReset.getPresets()) {
			
			presetList += preset.getName() + ", ";
			
		}
		
		presetList = presetList.trim();
		
		Messenger.resultMessage(player, "Here are all the presets: " + presetList);
		
		
	}

	
	
}
