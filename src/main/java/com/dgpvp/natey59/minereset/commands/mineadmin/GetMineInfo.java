package com.dgpvp.natey59.minereset.commands.mineadmin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class GetMineInfo extends MineResetCommand {

	public GetMineInfo(MineReset mineReset) {
		super(mineReset, "info");
		this.setPermission(Permissions.mineAdmin);
		this.setParameterCount(0);
		this.setUsage("/mr info");
		this.setDescription("Get all information about a mine.");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		Player player = (Player) sender;
		
		Mine mine = MineSelector.getMinePlayerIsOn(mineReset, player);
		
		if (mine != null) {
			Messenger.sendMineInfo(player, mine);
		} else {
			Messenger.errorMessage(player, "You are not standing on a mine.");
		}
		
	}

}
