package com.dgpvp.natey59.minereset.commands.selector;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineID;
import com.dgpvp.natey59.minereset.mine.utils.MineParser;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class MineSelector extends MineResetCommand {

	public static HashMap<UUID, MineID> selectedMines = new HashMap<UUID, MineID>();
	
	public MineSelector(MineReset mineReset) {
		super(mineReset, "select");
		this.setPermission(Permissions.mineAdmin);
		this.setUsage("/mr select");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		Mine mine = MineSelector.getMinePlayerIsOn(mineReset, (Player) sender);
		
		Player player = (Player) sender;
		
		if (mine != null) {
			MineSelector.selectedMines.put(player.getUniqueId(), mine.getId());
			Messenger.resultMessage(player, "You have selected " + mine.getName());
		} else {
			Messenger.errorMessage(player, "You are either not standing on a mine or the mine you are standing on is unloaded (break a block in to load it)");
		}		
	}

	public static Mine getMinePlayerIsOn(MineReset mineReset, Player player) {
		
		Location lookupLocation = player.getLocation().add(0, -1, 0);
		Mine standingOn = null;
		
		for (Mine mine : mineReset.getMines()) {
			if (mine.getMineArea().includesPoint(lookupLocation.getBlockX(), lookupLocation.getBlockY(), lookupLocation.getBlockZ())) {
				standingOn = mine;
				break;
			}
		}
		
		return standingOn;
	}
	
	public static HashMap<UUID, MineID> getSelectedMines() {
		return selectedMines;
	}
	
	public static Mine getSelectedMineFor(MineReset mr, Player player) {
		Mine mine = null;
		
		MineID selectedMineId;
		
		selectedMineId = MineSelector.selectedMines.get(player);
		
		if (selectedMineId != null) {
			mine = MineParser.getMineByID(mr, selectedMineId);
		}
		
		if (mine == null) {
			mine = MineSelector.getMinePlayerIsOn(mr, player);
		}
		
		return mine;
	}
	
}