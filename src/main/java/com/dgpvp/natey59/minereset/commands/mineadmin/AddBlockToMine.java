package com.dgpvp.natey59.minereset.commands.mineadmin;

import java.util.Map.Entry;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.commands.executor.MineResetCommand;
import com.dgpvp.natey59.minereset.commands.selector.MineSelector;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.mine.utils.Preset;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class AddBlockToMine extends MineResetCommand {

	
	
	public AddBlockToMine(MineReset mineReset) {
		super(mineReset, "add");
		this.setUsage("/mr add <block> <percentage>");
		this.setParameterCount(2);
		this.setPermission(Permissions.mineAdmin);
		this.setDescription("Add a block to a mine.");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
				
		Player player = (Player) sender;
		
		Mine mine = MineSelector.getSelectedMineFor(mineReset, player);
		
		if (mine != null) {
			
			if (args[0].contains("-ps:")) {
				String presetString = args[0].replace("-ps:", "");
				
				Preset preset = mineReset.getPresetByName(presetString);
				
				for (Entry<MineMaterial, Double> blocks : mine.getBlocks().entrySet()) {
					
					mine.removeBlock(blocks.getKey());
					
				}
				
				for (Entry<MineMaterial, Double> item : preset.getMap().entrySet()) {
					
					mine.addBlock(item.getValue(), item.getKey());
					
				}
				
				Messenger.resultMessage((Player)sender, "This mine is now using the preset " + preset.getName());
								
				return;
			}
			
			try {
				MineMaterial materialToAdd = MineMaterial.parse(args[0]);
				Double percentage = Double.parseDouble(args[1]);
					
				mine.addBlock(percentage, materialToAdd);
					
				Messenger.resultMessage(player, materialToAdd.toString() + " has been added to " + mine.getName());
					
			} catch (MaterialNotFoundException e) {
				Messenger.errorMessage(player, "You entered an invalid material or percentage");
			}
			
		} else {
			Messenger.errorMessage(player, "You do not have a mine selected. This is becuase you are either not standing on one, you didn't do /mr select <mine>, "
					+ "or the mine you are standing on has not been enabled. To enable it just break a block inside of it.");
		}
		
		
	}

	
	
}
