/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.mine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.mine.utils.MineID;
import com.dgpvp.natey59.minereset.mine.utils.MineMaterial;
import com.dgpvp.natey59.minereset.mine.utils.PrimitiveCuboid;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class Mine implements Serializable {

	private static final long serialVersionUID = -8888855475432042334L;

	private MineID id;
	
	private String name;
	
	private String owner;
	
	private String creator;
	
	private PrimitiveCuboid area;
	
	private int timeTillReset;
	
	private List<String> notes = new ArrayList<String>();
	
	private List<Mine> childrenMines = new ArrayList<Mine>();
		
	private Mine parentMine = null;
	
	public HashMap<String, Double> blocks = new HashMap<String, Double>(); // Block, Percentage of mine.
	
	private String world;
	
	public boolean isDisabled = false;
	
	private int[] warp = new int[3];
	
	public Mine(MineID id, String name, OfflinePlayer owner, String creator, int timeTillReset, PrimitiveCuboid area, Location warp) {
		
		this.id = id;
		this.name = name;
		this.owner = owner.getName();
		this.area = area;
		this.timeTillReset = timeTillReset*20;
		
		this.warp[0] = warp.getBlockX();
		this.warp[1] = warp.getBlockY()+1;
		this.warp[2] = warp.getBlockZ();
		
		world = warp.getWorld().getName();
		
	}
	
	public MineID getId() {
		return this.id;
	}
		
	public String getCreator() {
		return this.creator;
	}
	
	public String getName() {
		return this.name;
	}
	
	@SuppressWarnings("deprecation")
	public OfflinePlayer getOwner() {
		return Bukkit.getOfflinePlayer(owner);
	}
	
	public void setOWner(Player player) {
		this.owner = player.getName();
	}
		
	public PrimitiveCuboid getMineArea() {
		return this.area;
	}
	
	public List<String> getNotes() {
		return this.notes;
	}
	
	public void addNote(String note) {
		this.notes.add(note);
	}
	
	public void setTimeToReset(int time) {
		this.timeTillReset = time;
	}
	
	public int getTimeToReset() {
		return this.timeTillReset;
	}
			
	/**
	 * Add a block to a mine. <br>
	 * Weather or not a block name is valid needs to be determined before it is run through this method.
	 * 
	 * @param percentage
	 * @param block
	 * @return false if percentage is invalid
	 */
	public boolean addBlock(double percentage, MineMaterial block) {		
		double percentageUsed = this.getPercentageOfMineUsed();
		
		if ((percentageUsed + percentage) > 100) {
			return false;
		} else {
			this.blocks.put(block.toString(), percentage);
			return true;
		}
	}
	
	public HashMap<MineMaterial, Double> getBlocks() {
		HashMap<MineMaterial, Double> blocks = new HashMap<MineMaterial, Double>();
		
		for (Entry<String, Double> item : this.blocks.entrySet()) {
			MineMaterial material;
			try {
				material = MineMaterial.parse((String)item.getKey());
				
				if (material != null) {
					blocks.put(material, item.getValue());
				}
			} catch (MaterialNotFoundException e) {
				e.printStackTrace();
			}			
		}
		
		return blocks;
	}
	
	public double getPercentageOfMineUsed() {
		
		double percentage = 0.0;
		
		for (Entry<String, Double> block : this.blocks.entrySet()) {
			
			percentage += block.getValue();
			
		}
		
		return percentage;
		
	}
	
	public double getUnusedPercentageOfMine() {
		
		return 100 - this.getPercentageOfMineUsed();
		
	}
	
	public void removeBlock(MineMaterial block) {
		this.blocks.remove(block.toString());
	}

	public Location getWarp() {
		Location warp = null;
		
		long[] highest = this.area.xyzB;
		
		warp = new Location(Bukkit.getWorld(this.world), highest[0], highest[1]+1, highest[2]);
		
		return warp;
	}
	
	public void addChildMine(Mine mine) {
		this.childrenMines.add(mine);
	}
	
	public List<Mine> getChildrenMines() {
		return this.childrenMines;
	}
	
	public void setParentMine(Mine mine) {
		this.parentMine = mine;
	}
	
	public Mine getParentMine() {
		return this.parentMine;
	}
	
	public boolean hasParentMine() {
		if (this.getParentMine() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasChildrenMines() {
		if (this.getChildrenMines() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	@SuppressWarnings("deprecation")
	public void reload(boolean forceReload) {		
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			
			if (this.getMineArea().includesPoint(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())) {
				player.teleport(this.getWarp());
			}
			
		}
				
		List<MaterialCompisition> probabilityMap = getComp(this.getBlocks());
		
		Random rand = new Random();
		
		long[] xyzA = this.area.xyzA;
		long[] xyzB = this.area.xyzB;
		World world = Bukkit.getWorld(this.world);
		
		for(long x = xyzA[0]; x <= xyzB[0]; x++){
    		for(long y = xyzA[1]; y <= xyzB[1]; y++){
    			for(long z = xyzA[2]; z <= xyzB[2]; z++){
    				    				
    				double rando = rand.nextDouble();
    				
    				for (MaterialCompisition ce : probabilityMap) {
                        if (rando <= ce.getChance()) {
                            world.getBlockAt((int)x, (int)y, (int)z).setTypeIdAndData(ce.getBlock().getType().getId(), ce.getBlock().getData(), false);
                            break;
                        }
                    }
    				
    			}
    		}
    	}
				
		
		if (this.hasChildrenMines()) {
			
			for (Mine child : this.getChildrenMines()) {
				child.reload(true);
			}
			
		}
				
		Messenger.broadcast(this.getName() + " has been reset.", Permissions.mineReset);
		
	}
		
	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isDisabled() {
		return isDisabled;
	}
	
	public void convert() throws MaterialNotFoundException {
		
		HashMap<String, Double> converted = new HashMap<String, Double>();
		
		for (Entry<String, Double> item : this.blocks.entrySet()) {
		
			MineMaterial itemAsMaterial = MineMaterial.parse(item.getKey());
				
			converted.put(itemAsMaterial.toString(), item.getValue());
			
		}
		
		this.blocks = converted;
		
		
	}
	
	@SuppressWarnings("deprecation")
	public static ArrayList<MaterialCompisition> getComp(Map<MineMaterial, Double> compositionIn) {
        ArrayList<MaterialCompisition> probabilityMap = new ArrayList<MaterialCompisition>();
        
        Map<MineMaterial, Double> composition = new HashMap<MineMaterial, Double>(compositionIn);
        
        double max = 0;
        
        for (Map.Entry<MineMaterial, Double> entry : composition.entrySet()) {
            max += entry.getValue();
        }
        
        
        if (max < 1) {
            composition.put(new MineMaterial(Material.getMaterial(0), (byte)0), 1 - max);
            max = 1;
        }
        
        double i = 0;
        
        for (Map.Entry<MineMaterial, Double> entry : composition.entrySet())  {
            double v = entry.getValue() / max;
            i += v;
            probabilityMap.add(new MaterialCompisition(entry.getKey(), i));
        }
        
        return probabilityMap;
    }
	
	@Override
	public String toString() {
		return this.getName() + "_" + this.getId().toString() + "_" + this.getMineArea().toString() + "_" + this.world + "_" + this.owner + "_" + this.timeTillReset;
	}
	
	public static class MaterialCompisition {
        private MineMaterial block;
        private double chance;

        public MaterialCompisition(MineMaterial block, double chance) {
            this.block = block;
            this.chance = chance;
        }

        public MineMaterial getBlock() {
            return block;
        }

        public double getChance() {
            return chance;
        }
    }
	
}