package com.dgpvp.natey59.minereset.mine.flags.handler;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.flags.handler.FlagHandler.RunPoint;

public abstract class Flag implements Serializable {

	private static final long serialVersionUID = -7637650832432293101L;
	
	private String name;
	
	private Mine mine;
	
	public Flag(String name) {
		this.name = name;
	}
	
	public void addMine(Mine mine) {
		this.mine = mine;
	}
	
	public void runPreReset() {
		this.runMethodsWith(RunPoint.PRE_RESET);
	}
	
	public void runPostReset() {
		this.runMethodsWith(RunPoint.POST_RESET);
	}
	
	public void runOnCreation() {
		this.runMethodsWith(RunPoint.CREATION);
	}
	
	public void runOnDeletion() {
		this.runMethodsWith(RunPoint.ON_DELETION);
	}

	public String getName() {
		return name;
	}
	
	public Mine getMine() {
		return this.mine;
	}
		
	private void runMethodsWith(RunPoint runPoint) {
		
		Class<? extends Flag> obj = this.getClass();
		
		for (Method method : obj.getMethods()) {
						
			if (method.isAnnotationPresent(FlagHandler.class)) {
								
				Annotation annotation = method.getAnnotation(FlagHandler.class);
				
				FlagHandler handler = (FlagHandler) annotation;
				
				if (handler.runPoint() == runPoint) {
					
					try {
						method.invoke(obj.newInstance());
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | InstantiationException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			
		}
		
	}
}
