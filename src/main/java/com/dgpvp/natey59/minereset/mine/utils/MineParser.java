/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.mine.utils;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.OfflinePlayer;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.runnables.LoadResetingMine;

public class MineParser {

	private MineReset plugin;
	
	public MineParser(MineReset instance) {
		this.plugin = instance;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Mine> loadMines() {
		try {
			
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(plugin.getMineFile()));
			
			Object output = inputStream.readObject();
			inputStream.close();
			
			if (output instanceof ArrayList<?>) {
				ArrayList<Mine> mines = (ArrayList<Mine>) output;
					
				return (ArrayList<Mine>) mines;				
			}
			
		} catch (Exception e) {
			
			return new ArrayList<Mine>();
		}
		
		return new ArrayList<Mine>();
	}
	
	public static Mine getMineByID(MineReset plugin, MineID id) {
		
		Mine result = null;
		
		for (Mine mine : plugin.getMines()) {
			
			if (mine.getId() == id) {
				result =  mine;
			}
			
		}
		
		return result;
		
	}
	
	public static Mine getMineByName(MineReset plugin, String name) {
		
		Mine result = null;
		
		for (Mine mine : plugin.getMines()) {
			
			if (mine.getName().equals(name)) {
				result = mine;
			}
			
		}
		
		return result;
	}

	public void loadMineResets() {
	    List<Mine> mines = this.plugin.getMines();
	    
	    
	    
	    Random rand = new Random();
	    int range = 229;
	    for (Mine mine : mines) {
	      long delay = (rand.nextInt(range) + 12) * 20;
	      
	      BukkitRunnable loader = new LoadResetingMine(this.plugin, mine);
	      
	      loader.runTaskLater(this.plugin, delay);
	      
	      this.plugin.getLogger().info(mine.getName() + " will be loaded in " + delay / 20L + " seconds");
	    }
	  }
	
	public List<Mine> getPlayersMines(OfflinePlayer player) {
		List<Mine> mines = new ArrayList<Mine>();
		
		for (Mine mine : plugin.getMines()) {
			if (mine.getOwner().equals(player)) {
				mines.add(mine);
			}
		}
		
		return mines;
		
	}	
	
	
}
