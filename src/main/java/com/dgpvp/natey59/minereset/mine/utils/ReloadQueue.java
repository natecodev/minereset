package com.dgpvp.natey59.minereset.mine.utils;

import java.util.ArrayList;
import java.util.List;

import com.dgpvp.natey59.minereset.mine.Mine;

public class ReloadQueue {

	private List<Mine> toBeLoaded = new ArrayList<Mine>();
	
	public void addToQueue(Mine mine) {
		this.toBeLoaded.add(mine);
	}
	
	public void reloadNext() {	
		
		if (this.hasNext()) {
			toBeLoaded.get(0).reload(false);
			toBeLoaded.remove(0);
		}
	}
	
	private boolean hasNext() {
		if (this.toBeLoaded.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
}
