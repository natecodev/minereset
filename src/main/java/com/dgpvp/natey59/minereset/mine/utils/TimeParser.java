package com.dgpvp.natey59.minereset.mine.utils;

public class TimeParser {

	/**
	 * s = seconds
	 * m = minutes
	 * h = hours
	 * d = days 
	 */
	public static Long parseStringToLong(String timeAsString) {
		long length = 0;
		
		long amountFromString = 0;
		String unit = "";
		
		String[] input = timeAsString.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		
		amountFromString = Long.parseLong(input[0]);
		unit = input[1];
		
		switch (unit.toUpperCase()) {
		
		case "S":
			length = amountFromString*20;
			break;
		case "M":
			length = amountFromString*60*20;
			break;
		case "H":
			length = amountFromString*60*60*20;
			break;
		case "D":
			length = amountFromString*24*60*20;
			break;
		default:
			length = amountFromString*20;
		}
		
		return length;
	}
	
}
