/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.mine.utils;

/**
 * This is here to allow us to create a YAML safe UUID while maintaining all the usability of a UUID.
 * @author Nate Young
 */

import java.io.Serializable;
import java.util.UUID;

public class MineID implements Serializable {

	private static final long serialVersionUID = 4745889288666343572L;
	
	private String id;
	
	private MineID(UUID uuid) {
		
		String uuidAsString = uuid.toString();
		uuidAsString = uuidAsString.replace("-", "");
		
		this.id = uuidAsString;
		
	}
	
	public static MineID generateMineId() {
		return new MineID(UUID.randomUUID());
	}
	
	public static MineID generateMineIdFromString(String salt) {
		return new MineID(UUID.fromString(salt));
	}
	
	public String toString() {
		return id;
	}
	
}
