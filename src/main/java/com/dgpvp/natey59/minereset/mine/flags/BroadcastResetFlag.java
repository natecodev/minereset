package com.dgpvp.natey59.minereset.mine.flags;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.flags.handler.Flag;
import com.dgpvp.natey59.minereset.mine.flags.handler.FlagHandler;
import com.dgpvp.natey59.minereset.mine.flags.handler.FlagHandler.RunPoint;


public class BroadcastResetFlag extends Flag {
	
	private static final long serialVersionUID = -8750124157055952144L;

	public BroadcastResetFlag() {
		super("broadcast");
	}

	@FlagHandler(runPoint = RunPoint.POST_RESET)
	public void broadcastReset(Mine mine) {
		Bukkit.broadcastMessage(ChatColor.GREEN + this.getMine().getName() + " has been reset");
	}
	
	@FlagHandler(runPoint = RunPoint.CREATION)
	public void broadcastCreation(Mine mine) {
		Bukkit.broadcastMessage(ChatColor.GREEN + mine.getName() + " has been created");
	}
	
	
}
