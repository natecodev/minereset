package com.dgpvp.natey59.minereset.mine.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.util.com.google.gson.JsonObject;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;

public class Preset {
	
	private String name;
	private List<MineMaterial> materials;
	
	public Preset(String name, List<MineMaterial> materials) {
		this.materials = materials;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<MineMaterial> getMaterials() {
		return this.materials;
	}
	
	public HashMap<MineMaterial, Double> getMap() {
		
		HashMap<MineMaterial, Double> map = new HashMap<MineMaterial, Double>();
		
		double probability = 100/this.materials.size();
		
		for (MineMaterial item : this.materials) {
			
			map.put(item, probability);
			
		}
		
		return map;
		
	}
	
	public static List<Preset> loadPresets(MineReset mr) throws MaterialNotFoundException {
		
		List<Preset> presets = new ArrayList<Preset>();
		
		List<String> unparsed = mr.getConfig().getStringList("presets");
		
		for (String unparsedPreset : unparsed) {
			
			Preset parsedPreset = Preset.parseFromConfig(unparsedPreset);
			
			presets.add(parsedPreset);
			
		}
		
		return presets;
		
	}
	
	//Example of format: 'a_woody{[17, 17:1, 17:2, 17:3, 162, 162:1]}'
	public static Preset parseFromConfig(String presetAsText) throws MaterialNotFoundException {
		String name = "";
		
		name = presetAsText.substring(0, presetAsText.indexOf('{'));
		
		List<MineMaterial> items = new ArrayList<MineMaterial>();
		
		String bulk = presetAsText.replace(name, "");
		
		bulk = bulk.replace("{", "");
		bulk = bulk.replace("}", "");
		bulk = bulk.replace("[", "");
		bulk = bulk.replace("]", "");
		bulk = bulk.replace("\"", "");
		
		String[] data = bulk.split(",");
		
		for (String string : data) {
			
			MineMaterial material = MineMaterial.parse(string);
				
			if (material != null) {
				items.add(material);
			}
			
			
		}
		
			
		
		return new Preset(name, items);
	}
	
	public String toString() {
		
		String materials = this.materials.toString();
		
		return this.name + "{" + materials + "}";
		
	}
	
	public JsonObject serialize() {	
		
		JsonObject json = new JsonObject();
		
		json.addProperty("name", this.name);
		
		String items = this.materials.toString();
		
		json.addProperty("items", items);
		
		return json;
		
	}
	
	
}
