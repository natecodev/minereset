package com.dgpvp.natey59.minereset.mine.utils;

import java.io.Serializable;

import org.bukkit.Material;

import com.dgpvp.natey59.minereset.exceptions.MaterialNotFoundException;

public class MineMaterial implements Serializable {

	private static final long serialVersionUID = 5844915161167538770L;
	
	private String type;
	private byte data;
	
	public MineMaterial(Material type, byte data) {
		this.type = type.name();
		this.data = data;
	}
	
	public Material getType() {
		return Material.getMaterial(type);
	}
	
	public byte getData() {
		return this.data;
	}

	@SuppressWarnings("deprecation")
	public static MineMaterial parse(String args) throws MaterialNotFoundException{
		
		MineMaterial mineMaterial = null;
		
		if (!args.contains(":")) {
			args += ":0";
		}
		
		String[] data = args.split(":");
		
		byte byteData;
				
		try {
			 byteData = Byte.parseByte(data[1]);
		} catch (NumberFormatException e) {
			Color color = Color.getByName(data[1]);
			
			if (color != null) {
				byteData = color.getId();
			} else {
				byteData = 0;
			}
		}
			
		data[0] = data[0].replace(" ", "");
		data[1] = data[1].replace(" ", "");
		
		Material type = Material.getMaterial(data[0].toUpperCase());
		
		if (type == null) {
			type = Material.getMaterial(Integer.parseInt(data[0]));
		}
		
		if (type != null && data != null && type.isBlock() && type.isSolid()) {
			
			mineMaterial = new MineMaterial(type, byteData);
			
		} else {
			throw new MaterialNotFoundException(args + " could not be found as a material");
		}
		
		return mineMaterial;
	}
	

	@Override
	public String toString() {
		return type + ":" + data;
	}
	
	public enum Color {
		
		WHITE((byte)0, "WHITE"),
		ORANGE((byte)1, "ORANGE"),
		MAGENTA((byte)2, "MAGENTA"),
		LIGHT_BLUE((byte)3, "LIGHT_BLUE"),
		YELLOW((byte)4, "YELLOW"),
		LIME((byte)5, "LIME"),
		PINK((byte)6, "PINK"),
		GRAY((byte)7, "GRAY"),
		LIGHT_GRAY((byte)8, "LIGHT_GRAY"),
		CYAN((byte)9, "CYAN"),
		PURPLE((byte)10, "PURPLE"),
		BLUE((byte)11, "BLUE"),
		BROWN((byte)12, "BROWN"),
		GREEN((byte)13, "GREEN"),
		RED((byte)14, "RED"),
		BLACK((byte)15, "BLACK");
		
		private byte byteId;
		private String readableName;
		
		private Color(byte byteId, String readableName) {
			this.byteId = byteId;
			this.readableName = readableName;
		}
		
		public byte getId() {
			return this.byteId;
		}
		
		public String getName() {
			return this.readableName;
		}
		
		public static Color getByName(String name) {
			
			Color returnColor = null;
			
			for (Color color : Color.values()) {
				if (color.getName().equalsIgnoreCase(name)) {
					returnColor = color;
				}
			}
			
			return returnColor;
			
		}
		
	}
	
}