package com.dgpvp.natey59.minereset.mine.flags;

import com.dgpvp.natey59.minereset.mine.Mine;
import com.dgpvp.natey59.minereset.mine.flags.handler.Flag;
import com.dgpvp.natey59.minereset.mine.flags.handler.FlagHandler;
import com.dgpvp.natey59.minereset.mine.flags.handler.FlagHandler.RunPoint;


public class PreMadeWallFlag extends Flag {
	
	private static final long serialVersionUID = -2715727742546888604L;

	public PreMadeWallFlag(String[] args) {
		super("wall");
	}
	
	@FlagHandler(runPoint = RunPoint.CREATION)
	public void createWall(Mine mine) {
		
		
		
	}
	
	
	
}
