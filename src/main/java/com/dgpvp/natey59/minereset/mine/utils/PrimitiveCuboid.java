/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.mine.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class PrimitiveCuboid implements Serializable {

	private static final long serialVersionUID = -6237564871795133663L;

	String worldName;
	
	public long[] xyzA = { 0, 0, 0 };
	public long[] xyzB = { 0, 0, 0 };
	long lowIndex[] = new long[3];
	long highIndex[] = new long[3];
	
	private int hashcode = 0;
	
	/**
	 * Normalize the corners so that all A is <= B This is CRITICAL to have for
	 * comparison to a point
	 */
	final private void normalize() {
		for (int i = 0; i < 3; i++) {
			if (xyzA[i] > xyzB[i]) {
				long temp = xyzA[i];
				xyzA[i] = xyzB[i];
				xyzB[i] = temp;
			}
			hashcode ^= xyzB[i] ^ (~xyzA[i]);
		}
	}
	
	public PrimitiveCuboid(Location min, Location max) {
				
		xyzA[0] = min.getBlockX();
		xyzA[1] = min.getBlockY();
		xyzA[2] = min.getBlockZ();
		
		xyzB[0] = max.getBlockX();
		xyzB[1] = max.getBlockY();
		xyzB[2] = max.getBlockZ();
		
		this.worldName = min.getWorld().getName();
		
		this.normalize();
		
	}
	
	public PrimitiveCuboid(long[] min, long[] max, String world) {
		
		xyzA[0] = min[0];
		xyzA[1] = min[1];
		xyzA[2] = min[2];
		
		xyzB[0] = max[0];
		xyzB[1] = max[1];
		xyzB[2] = max[2];
		
		this.worldName = world;
		
		this.normalize();
	}


	public boolean includesPoint(long x, long y, long z) {
		if (xyzA[0] <= x && xyzA[1] <= y && xyzA[2] <= z && xyzB[0] >= x
				&& xyzB[1] >= y && xyzB[2] >= z) {
			return true;
		}
		return false;
	}

	public boolean includesPoint(long[] pt) {
		return this.includesPoint(pt[0], pt[1], pt[2]);
	}

	@Override
	public int hashCode() {
		return hashcode;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		} else if (!(o instanceof PrimitiveCuboid)) {
			return false;
		}
		PrimitiveCuboid c = (PrimitiveCuboid) o;

		for (int i = 0; i < 3; i++) {
			if (xyzA[i] != c.xyzA[i]) {
				return false;
			}
			if (xyzB[i] != c.xyzB[i]) {
				return false;
			}
		}
		return true;
	}

	public boolean overlaps(PrimitiveCuboid c) {
		for (int i = 0; i < 3; i++) {
			if (xyzA[i] > c.xyzB[i] || c.xyzA[i] > xyzB[i]) {
				return false;
			}
		}
		return true;
	}
	
	public PrimitiveCuboid getTopLayer() {
		
		long[] cornerTwo = new long[3]; //Corner two consists of xyzB's x and z coordinates and so that we only get the top layer it's y is the same as xyzA's
		
		cornerTwo[0] = xyzA[0];
		cornerTwo[1] = xyzB[1];
		cornerTwo[2] = xyzA[2];
		
		PrimitiveCuboid topLayer = new PrimitiveCuboid(xyzB, cornerTwo, this.worldName);
		
		return topLayer;
	}
	
	public List<Block> getBlocksInCuboid() {
    	List<Block> blocks = new ArrayList<Block>();
    	
    	for(long x = xyzA[0]; x <= xyzB[0]; x++){
    		for(long y = xyzA[1]; y <= xyzB[1]; y++){
    			for(long z = xyzA[2]; z <= xyzB[2]; z++){
    				blocks.add(Bukkit.getWorld(worldName).getBlockAt((int)x, (int)y, (int)z));
    			}
    		}
    	}
    	
    	return blocks;
    }
	
	public List<Material> getTypesInCubiod() {
		List<Material> materials = new ArrayList<Material>();		
		
		for(long x = xyzA[0]; x <= xyzB[0]; x++){
    		for(long y = xyzA[1]; y <= xyzB[1]; y++){
    			for(long z = xyzA[2]; z <= xyzB[2]; z++){
    				materials.add(Bukkit.getWorld(worldName).getBlockAt((int)x, (int)y, (int)z).getType());
    			}
    		}
    	}
		
		return materials;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("xA: ").append(xyzA[0])
				.append(" yA: ").append(xyzA[1]).append(" zA: ")
				.append(xyzA[2]).append(" * xB: ").append(xyzB[0])
				.append(" yB: ").append(xyzB[1]).append(" zB: ")
				.append(xyzB[2]).toString();
	}
}