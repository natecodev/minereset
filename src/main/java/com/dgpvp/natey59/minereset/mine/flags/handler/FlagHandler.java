package com.dgpvp.natey59.minereset.mine.flags.handler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) 
public @interface FlagHandler {

	public enum RunPoint {
		CREATION, PRE_RESET, POST_RESET, ON_DELETION;
	}
		
	public RunPoint runPoint();
	
}
