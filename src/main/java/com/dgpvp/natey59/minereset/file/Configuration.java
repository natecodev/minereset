/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Configuration {

	
	private JavaPlugin plugin;
	private File file;
	private FileConfiguration config;
	private String configName;
		
	public Configuration(JavaPlugin instance, File file, FileConfiguration config, String configName) {
		plugin = instance;
		
		this.file = file;
		this.config = config;
		this.configName = configName;
	}
	
	public void reloadConfig(){
		if (file == null){
			file = new File(plugin.getDataFolder(), configName);
		}
		config = YamlConfiguration.loadConfiguration(file);
		
		InputStream defConfigStream = plugin.getResource(configName);
		if (defConfigStream != null){
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(file);
			config.setDefaults(defConfig);
		}
	}
	
	public FileConfiguration getConfig(){
		if (config == null){
			this.reloadConfig();
		}
		
		return config;
	}
	
	public void save(){
		if (config == null || file == null){
			return;
		}
		
		try{
			this.getConfig().save(file);
		}catch(IOException e){
			plugin.getLogger().log(Level.SEVERE, "Could not save config to " + file, e);
		}
	}
	
	public void saveDefualtConfiguration(){
		if (file == null){
			file = new File(plugin.getDataFolder(), configName);
		}
		if (!file.exists()){
			plugin.saveResource(configName, false);
		}
	}
}
