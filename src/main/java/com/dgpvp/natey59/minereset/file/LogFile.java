/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */

package com.dgpvp.natey59.minereset.file;

import java.io.*;
import java.util.ArrayList;

public class LogFile {

	private File storeageFile;
	private ArrayList<String> values;

	public LogFile(File file) {
		storeageFile = file;
		values = new ArrayList<String>();
		storeageFile.getParentFile().mkdirs();
		if (!storeageFile.exists()) {
			try {
				storeageFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void load() {
		try {
			DataInputStream input = new DataInputStream(new FileInputStream(
					storeageFile));
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					input));
			String line;
			while ((line = reader.readLine()) != null) {
				if (values.contains(line)) {
					values.add(line);
				}
			}
			reader.close();
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			FileWriter stream = new FileWriter(storeageFile);
			BufferedWriter out = new BufferedWriter(stream);
			for (String value : this.values) {
				out.write(value);
				out.newLine();
			}

			out.close();
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean contains(String value) {
		return values.contains(value);
	}

	public void add(String value) {
		if (!contains(value)) {
			values.add(value);
		}
	}

	public void remove(String value) {
		values.remove(value);
	}

	public ArrayList<String> getValues() {
		return values;
	}
}