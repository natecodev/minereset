package com.dgpvp.natey59.minereset.file.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.minereset.MineReset;
import com.dgpvp.natey59.minereset.language.Messenger;
import com.dgpvp.natey59.minereset.permissions.Permissions;

public class Backup extends BukkitRunnable {

	private MineReset mineReset;
	
	public Backup(MineReset mineReset) {
		this.mineReset = mineReset;
	}
	
	@Override
	public void run() {
		
		Messenger.broadcast("Force saving mines...", Permissions.mineBackup);
		
		try {
			mineReset.saveMines();
			Messenger.broadcast("All mines have been saved.", Permissions.mineBackup);
		} catch (Exception e) {
			Messenger.broadcast("Mines failed to save.", Permissions.mineBackup);
		}
		
		File backupLocation = new File(mineReset.getDataFolder(), File.separator + "backups");
		
		if (!backupLocation.exists()) {
			backupLocation.mkdir();
		}
		
		String timeStamp = new SimpleDateFormat("MM-dd-yyyy;HH-mm-ss").format(Calendar.getInstance().getTime());
		
		File fileToBackup = mineReset.getMineFile();
		File fileBackup = new File(backupLocation, timeStamp + ".mine.dat.backup");
		
		Messenger.broadcast("Creating backup...", Permissions.mineBackup);
		
		try {
			this.copyFile(fileToBackup, fileBackup);
			Messenger.broadcast("Backup complete.", Permissions.mineBackup);
		} catch (IOException e) {
			e.printStackTrace();
			Messenger.broadcast("Backup failed.", Permissions.mineBackup);
		}
		
	}
	
	public void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	
}
