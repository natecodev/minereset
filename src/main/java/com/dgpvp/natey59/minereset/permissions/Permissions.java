/**
 * @author Nate Young
 * This product is the sole work of Nate Young.
 * It is not to be distributed by any party other than Nate Young Development.
 * This was created for DgPVP LLC.
 * Unless you have written consent from Nate Young or DGPVP to copy and use this program YOU ARE NOT PERMITTED TO.
 * If this product is taken and used without the written consent of Nate Young or DGPVP legal action can and will be taken against all offenders.
 * This product comes with limited warranty and an expectation of full functionality when used by DGPVP.
 * This product comes with no warranty and no expectation of full functionality when used by any party other than DGPVP.
 * Copyright Nate Young 2014
 */
package com.dgpvp.natey59.minereset.permissions;

import org.bukkit.permissions.Permission;

public class Permissions {

	private Permissions() { }
	
	public static Permission createMine = new Permission("minereset.createmine");
	public static Permission mineUser = new Permission("minereset.user");
	public static Permission mineAdmin = new Permission("minereset.admin");
	public static Permission createPreset = new Permission("minereset.preset");
	public static Permission backup = new Permission("minereset.backup");
	public static Permission mineReset = new Permission("minereset.spam.reset");
	public static Permission mineQueue = new Permission("minereset.spam.queue");
	public static Permission mineBackup = new Permission("minereset.spam.backup");
	public static Permission console = new Permission("minereset.console");
	
}
