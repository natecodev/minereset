function removeDummy(id) {
	var elem = document.getElementById(id);
    elem.parentNode.removeChild(elem);
	elem.class = 'deleted';
    return false;
}