# README #

## About
This plugin was created for the DGPVP network in order to add a better optimized and easier MineReset option.

## Suggestions
This plugin is made to be the best possible MineReset plugin that is tailored to the DGPVP admin community. So if you have anything you want in this plugin email natey59@gmail.com and I will look into implementing the features you want.*

## Issues / Bugs
If you would like to report an issue please create an account on this website and on the left side click the tab that says "Issues" and make an issue report.

## Guide
A full usage guide can be found in the tab on the left labeled "Wiki".